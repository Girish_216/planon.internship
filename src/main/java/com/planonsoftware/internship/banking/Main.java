package com.planonsoftware.internship.banking;

import com.planonsoftware.internship.banking.user.Input;
import com.planonsoftware.internship.banking.user.Menu;
import com.planonsoftware.internship.banking.user.User;

public class Main {

	public static void main(String[] args) {

		Input input = new Input();
		User user = new User();
		user.initializeAccounts();
		while (true) {
			if (user.validateUser(input.getUserName(), input.getPassword())) {
				break;
			} else {
				System.out.println("Invalid credentials");
				System.out.println("Please enter again");
			}
		}

		Menu menu = new Menu();
		while (true) {
			menu.printMenu();
			int ch = input.getChoice();
			switch (ch) {
				case 1:   menu.createAccount(input, user);   break;
				case 2:   menu.retriveAccount(input, user);  break;
				case 3:   menu.Transaction(input, user); 	 break;
				case 4:   menu.deleteAccount(input, user); 	 break;
				case 5:   menu.createUser(input, user); 	 break;
				case 6:   System.exit(0);
				default:  System.out.println("Enter a valid choice");
			}
		}
	}
}
