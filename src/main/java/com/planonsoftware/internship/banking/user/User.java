package com.planonsoftware.internship.banking.user;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.planonsoftware.internship.banking.model.AccountType;
import com.planonsoftware.internship.banking.model.BankAccount;
import com.planonsoftware.internship.banking.model.CurrentAccount;
import com.planonsoftware.internship.banking.model.IBankAccount;
import com.planonsoftware.internship.banking.model.SavingsAccount;
import com.planonsoftware.internship.banking.model.TransactionType;

/**
 * {@code User} class is used to manipulate all users and account operations
 * 
 * @author Girish Gandham
 *
 */
public class User {

	/**
	 * list of user credentials(username & password)
	 */
	private final Map<String, String> credentials;
	/**
	 * list of bank accounts
	 */
	private List<IBankAccount> accounts;

	/**
	 * constructs user
	 */
	public User() {
		this.credentials = new HashMap<>();
		this.accounts = new ArrayList<IBankAccount>();
	}

	/**
	 * initializing sample accounts
	 */
	public void initializeAccounts() {
		createUser("planon", "planon123");
		createUser("admin", "admin123");
		createAccount(123456789, "Girish", AccountType.SAVINGS, 10000);
		createAccount(789456123, "Girish", AccountType.CURRENT, 10000);
	}

	/**
	 * creates new users
	 * 
	 * @param uname    user name of the new user
	 * @param password password of new user
	 */
	public void createUser(String uname, String password) {
		credentials.put(uname, password);
	}

	/**
	 * validates the given user credentials
	 * 
	 * @param uname
	 * @param password
	 * @return true if valid user
	 */
	public boolean validateUser(String uname, String password) {
		return (credentials.containsKey(uname) && password.equals(credentials.get(uname)));
	}

	/**
	 * creates new Bank account
	 * 
	 * @param accountNum  Account Number
	 * @param name        Account Holder name
	 * @param accountType Type of Account
	 * @param balance     Balance
	 */
	public void createAccount(long accountNum, String name, AccountType accountType, double balance) {
		switch (accountType) {
		case SAVINGS:
			accounts.add(new SavingsAccount(accountNum, name, accountType, balance));
			break;
		case CURRENT:
			accounts.add(new CurrentAccount(accountNum, name, accountType, balance));
			break;
		default:
			System.out.println("Invalid Account Type");
		}
	}

	/**
	 * returns the bank account of the given account number
	 * 
	 * @param accountNum
	 * @return {@code IBankAccount} object
	 */
	public IBankAccount getAccount(long accountNum) {
		return accounts.stream().filter((a) -> ((BankAccount) a).getAccNum() == accountNum).findFirst().orElse(null);
	}

	/**
	 * prints bank account details of given account number
	 * 
	 * @param accountNum
	 */
	public void printAccountDetails(long accountNum) {
		IBankAccount account = getAccount(accountNum);
		assert account != null : "Invalid account number";
		account.printDetails();
	}

	/**
	 * used to perform transaction based on given transaction type
	 * 
	 * @param accountNum
	 * @param transactionType
	 * @param amount
	 */
	public void performTransaction(long accountNum, TransactionType transactionType, double amount) {
		IBankAccount account = getAccount(accountNum);
		assert account != null : "Invalid account number";
		account.transaction(transactionType, amount);
	}

	/**
	 * removes account of the specified account number
	 * 
	 * @param accountNum
	 */
	public void deleteAccount(long accountNum) {
		IBankAccount account = getAccount(accountNum);
		assert account != null : "Invalid account number";
		accounts.remove(account);
		System.out.println("Account Removed");
	}
}
