package com.planonsoftware.internship.banking.user;

/**
 * {@code Menu} is used to print menu and perform operations in menu
 * 
 * @author Girish Gandham
 *
 */
public class Menu {
	/**
	 * prints the menu
	 */
	public void printMenu() {
		System.out.println("Menu: ");
		System.out.println("1.Create a Bank Account");
		System.out.println("2.Retrive Account Details");
		System.out.println("3.Transaction");
		System.out.println("4.Delete a Bank Account");
		System.out.println("5.Create new User");
		System.out.println("6.Exit");
	}

	/**
	 * creates new Account
	 * 
	 * @param input input object
	 * @param user  user object
	 */
	public void createAccount(Input input, User user) {
		System.out.println("\nCreating a new Account....");
		user.createAccount(input.getAccountNumber(), input.getName(), input.getAccountType(), input.getBalance());
		System.out.println("Account Created Successfully....");
	}

	/**
	 * Retrieves and prints the bank account details
	 * 
	 * @param input input object
	 * @param user  user object
	 */
	public void retriveAccount(Input input, User user) {
		user.printAccountDetails(input.getAccountNumber());
	}

	/**
	 * performs transaction
	 * 
	 * @param input input object
	 * @param user  user object
	 */
	public void Transaction(Input input, User user) {
		user.performTransaction(input.getAccountNumber(), input.getTransactionType(), input.getAmount());
	}

	/**
	 * deletes bank account
	 * 
	 * @param input input object
	 * @param user  user object
	 */
	public void deleteAccount(Input input, User user) {
		user.deleteAccount(input.getAccountNumber());
	}

	/**
	 * creates a new user
	 * 
	 * @param input input object
	 * @param user  user object
	 */
	public void createUser(Input input, User user) {
		user.createUser(input.getUserName(), input.getPassword());
	}

}
