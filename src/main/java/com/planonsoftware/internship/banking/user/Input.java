package com.planonsoftware.internship.banking.user;

import java.util.Scanner;

import com.planonsoftware.internship.banking.model.AccountType;
import com.planonsoftware.internship.banking.model.TransactionType;

public class Input {

	public Scanner input;

	public Input() {
		input = new Scanner(System.in);
	}

	/**
	 * to take username from console
	 * 
	 * @return username
	 */
	public String getUserName() {
		System.out.println("Enter Username:");
		return input.nextLine();
	}

	/**
	 * to take password from console
	 * 
	 * @return password
	 */
	public String getPassword() {
		System.out.println("Enter Password:");
		return input.nextLine();
	}

	/**
	 * to take choice from console
	 * 
	 * @return choice
	 */
	public int getChoice() {
		System.out.println("Enter your choice:");
		return Integer.parseInt(input.nextLine());
	}

	/**
	 * to take Account number from console
	 * 
	 * @return Account number
	 */
	public long getAccountNumber() {
		System.out.println("Enter Account number:");
		return Long.parseLong(input.nextLine());
	}

	/**
	 * to take Account holder's name from console
	 * 
	 * @return Account holder's name
	 */
	public String getName() {
		System.out.println("Enter Account holder Name: ");
		return input.nextLine();
	}

	/**
	 * to take Account Type from console
	 * 
	 * @return Account Type
	 */
	public AccountType getAccountType() {
		System.out.println("Enter Account type");
		return AccountType.valueOf(input.nextLine().toUpperCase());
	}

	/**
	 * to take balance from console
	 * 
	 * @return balance
	 */
	public double getBalance() {
		System.out.println("Enter Balance: ");
		return Double.parseDouble(input.nextLine());
	}

	/**
	 * to take Transaction type from console
	 * 
	 * @return Transaction type
	 */
	public TransactionType getTransactionType() {
		System.out.println("Enter type of Transaction:");
		return TransactionType.valueOf(input.nextLine().toUpperCase());

	}

	/**
	 * to take amount from console
	 * 
	 * @return amount
	 */
	public double getAmount() {
		System.out.println("Enter Amount: ");
		return Double.parseDouble(input.nextLine());
	}
	
}
