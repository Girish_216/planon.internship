package com.planonsoftware.internship.banking.model;

/**
 * {@code BankAccount} is the base class for all accounts which implements
 * {@link IBankAccount} interface
 * 
 * @author Girish Gandham
 *
 */
public abstract class BankAccount implements IBankAccount {
	/**
	 * bank account number
	 */
	private final long accountNum;
	/**
	 * account holder name
	 */
	private final String name;
	/**
	 * bank account type
	 */
	private final AccountType accountType;
	/**
	 * bank account balance
	 */
	private double balance;

	/**
	 * constructs the bank account 
	 * 
	 * @param accNum
	 * @param name
	 * @param accType
	 * @param balance
	 */
	public BankAccount(long accountNum, String name, AccountType accountType, double balance) {
		this.accountNum = accountNum;
		this.name = name;
		this.accountType = accountType;
		this.balance = balance;
	}

	/**
	 * sets the balance of the account
	 * 
	 * @param balance
	 */
	public void setBalance(double balance) {
		if (balance > 1000)
			this.balance = balance;
		else {
			System.out.println("Insuffficient Balance!!!!");
			System.out.println("Account removed.....");
		}
	}

	/**
	 * returns Account number
	 * 
	 * @return AccountNumber
	 */
	public long getAccNum() {
		return accountNum;
	}

	/**
	 * returns Account holder name
	 * 
	 * @return Name
	 */
	public String getName() {
		return name;
	}

	/**
	 * returns Account Type
	 * 
	 * @return Account Type
	 */
	public AccountType getAccType() {
		return accountType;
	}

	/**
	 * returns Account Balance
	 * 
	 * @return Balance
	 */
	public double getBalance() {
		return balance;
	}

	@Override
	public void printDetails() {
		System.out.println("Account Number=" + accountNum + ", Account holder name=" + name + ", Account type=" + accountType
				+ ", Balance=" + balance);
	}

	/**
	 * checks for sufficient balance to perform transaction
	 * 
	 * @param amount to be checked
	 * @return true if balance is sufficient
	 */
	public boolean isSufficientBalance(double amount) {
		return amount < balance;
	}
}
