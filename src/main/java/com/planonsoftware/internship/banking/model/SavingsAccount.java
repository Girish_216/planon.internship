package com.planonsoftware.internship.banking.model;

/**
 * {@code SavingsAccount} is the base class for all saving accounts which
 * extends {@link BankAccount} class
 * 
 * @author Girish Gandham
 *
 */
public class SavingsAccount extends BankAccount {

	/**
	 * basic charges applied during transaction
	 */
	private static final double charges = 10;

	/**
	 * constructs the savings account
	 * 
	 * @param accountNum
	 * @param name
	 * @param accountType
	 * @param balance
	 */
	public SavingsAccount(long accountNum, String name, AccountType accountType, double balance) {
		super(accountNum, name, accountType, balance);
	}

	@Override
	public void transaction(TransactionType transactionType, double amount) {

		switch (transactionType) {
		case WITHDRAWAL:
			withdraw(amount);
			break;
		case DEPOSIT:
			deposit(amount);
			break;
		default:
			System.out.println("Invalid Transaction!!");
		}

	}

	@Override
	public void withdraw(double amount) {
		if (isSufficientBalance(amount)) {
			setBalance(getBalance() - amount - charges);
			System.out.println("Withdrawl Successfull!!!");
			System.out.println("Current Balance: " + getBalance());
		} else {
			System.out.println("Invalid Amount");
		}
	}

	@Override
	public void deposit(double amount) {
		setBalance(getBalance() + amount - charges);
		System.out.println("Deposit Successfull!!!");
		System.out.println("Current Balance: " + getBalance());
	}

}
