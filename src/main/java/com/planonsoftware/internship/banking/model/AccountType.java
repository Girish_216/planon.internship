package com.planonsoftware.internship.banking.model;

/**
 * {@code AccountType} enum is used to specify type of account
 * 
 * @author Girish Gandham
 *
 */
public enum AccountType {

	SAVINGS, CURRENT;
}
