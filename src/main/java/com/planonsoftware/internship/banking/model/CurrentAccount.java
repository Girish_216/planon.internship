package com.planonsoftware.internship.banking.model;

/**
 * {@code CurrentAccount} is the base class for all current accounts which
 * extends {@link BankAccount} class
 * 
 * @author Girish Gandham
 *
 */
public class CurrentAccount extends BankAccount {

	/**
	 * basic charges applied during transaction
	 */
	private static final double charges = 20;

	/**
	 * constructs the current account
	 * 
	 * @param accNum
	 * @param name
	 * @param accountType
	 * @param balance
	 */

	public CurrentAccount(long accountNum, String name, AccountType accountType, double balance) {
		super(accountNum, name, accountType, balance);
	}

	@Override
	public void transaction(TransactionType transactionType, double amount) {
		switch (transactionType) {
		case WITHDRAWAL:
			withdraw(amount);
			break;
		case DEPOSIT:
			deposit(amount);
			break;
		default:
			System.out.println("Invalid Transaction!!");
		}

	}

	@Override
	public void withdraw(double amount) {
		if (isSufficientBalance(amount)) {
			setBalance(getBalance() - amount - charges);
			System.out.println("Withdrawl Successfull!!!");
			System.out.println("Current Balance: " + getBalance());
		} else {
			System.out.println("Invalid Amount");
		}
	}

	@Override
	public void deposit(double amount) {
		setBalance(getBalance() + amount - charges);
		System.out.println("Deposit Successfull!!!");
		System.out.println("Current Balance: " + getBalance());
	}

}
