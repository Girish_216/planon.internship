package com.planonsoftware.internship.banking.model;
/**
 * {@code TransactionType} enum is used to specify type of transaction
 * @author Girish Gandham
 *
 */
public enum TransactionType {
	WITHDRAWAL,DEPOSIT;
}
