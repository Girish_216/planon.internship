package com.planonsoftware.internship.banking.model;

/**
 * The {@code IBankAccount} interface needs to be implemented by all the
 * accounts in the bank
 * 
 * @author Girish Gandham
 *
 */
public interface IBankAccount {

	/**
	 * prints the Account Details
	 *
	 */
	void printDetails();

	/**
	 * performs transaction based on transaction type
	 * 
	 * @param transactionType type of transaction to be performed
	 * @param amount
	 */
	void transaction(TransactionType transactionType, double amount);

	/**
	 * withdraws the given amount
	 * 
	 * @param amount amount to be withdrawn
	 */
	void withdraw(double amount);

	/**
	 * deposits the given amount
	 * 
	 * @param amount amount to be deposited
	 */
	void deposit(double amount);
}
