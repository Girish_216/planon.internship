package com.planonsoftware.internship.ecommerce.store;

/**
 * The {@code IProduct} interface needs to be implemented by all the products of
 * the Store
 * 
 * @author Girish Gandham
 *
 */
public interface IProduct {

	/**
	 * Returns {@code true} if productId is present
	 * 
	 * @param productId Id to be checked
	 * @return {@code true} if productId is present
	 */
	boolean isSameProduct(long productId);

	/**
	 * Returns {@code true} if quantity is present in store
	 * 
	 * @param quantity to be checked
	 * @return {@code true} if quantity is present
	 */
	boolean isSufficientQuantity(int quantity);
}
