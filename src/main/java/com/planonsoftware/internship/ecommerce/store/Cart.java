package com.planonsoftware.internship.ecommerce.store;

import java.util.HashMap;
import java.util.Map;

/**
 * {@code Cart} class is used to initialize the cart
 * 
 * @author Girish Gandham
 *
 */
public class Cart {

	private final String format = "%5s\t%20s\t%8s\t%5s\t%5s\n";
	/**
	 * cartProducts represents the list of products present in the cart
	 */
	private Map<IProduct, Integer> cartProducts;
	/**
	 * totalPrice represents the total price of all the products present in the cart
	 */
	private double totalAmount;

	/**
	 * constructs the Cart class
	 */
	public Cart() {
		cartProducts = new HashMap<>();
		totalAmount = 0;
	}

	/**
	 * returns list of products present in the cart
	 * 
	 * @return Cart products list
	 */
	public Map<IProduct, Integer> getCartProducts() {
		return cartProducts;
	}

	/**
	 * returns total amount of the cart
	 * 
	 * @return total amount
	 */
	public double getTotalAmount() {
		return totalAmount;
	}

	/**
	 * adding products to cart
	 * 
	 * @param product  product which is to be added
	 * @param quantity quantity of product which being added
	 */
	public void addToCart(IProduct product, int quantity) {
		if (cartProducts.containsKey(product)) {
			int q = cartProducts.get(product);
			cartProducts.put(product, quantity + q);
		} else
			cartProducts.put(product, quantity);
		totalAmount += (((Product) product).getPrice() * quantity);
	}

	/**
	 * Prints the details of products in cart
	 */
	public void checkOut() {
		if (cartProducts.isEmpty()) {
			System.out.println("Cart is Empty");
			return;
		}
		System.out.format(format, "ID", "Name", "Quantity", "Price", "Total Amount");
		cartProducts.keySet()
				.forEach((p) -> System.out.format(format, ((Product) p).getProductId(), ((Product) p).getProductName(),
						cartProducts.get(p), ((Product) p).getPrice(),
						(cartProducts.get(p) * ((Product) p).getPrice())));
		System.out.println("Total Amount to be paid: " + totalAmount);

	}
}
