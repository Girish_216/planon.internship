package com.planonsoftware.internship.ecommerce.store;

public class Product implements IProduct {

	/**
	 * {@code productId} represents the product ID
	 */
	private final long productId;
	/**
	 * {@code productName} represents the product name
	 */
	private final String productName;
	/**
	 * {@code quantity} represents the product quantity
	 */
	private int quantity;
	/**
	 * {@code price} represents the product price
	 */
	private double price;

	/**
	 * constructs the product class
	 * 
	 * @param productId
	 * @param productName
	 * @param quantity
	 * @param price
	 */
	public Product(long productId, String productName, int quantity, double price) {
		this.productId = productId;
		this.productName = productName;
		this.quantity = quantity;
		this.price = price;
	}

	/**
	 * returns Product ID of the product
	 * 
	 * @return Product ID
	 */
	public long getProductId() {
		return productId;
	}

	/**
	 * returns Product Name of the product
	 * 
	 * @return Product Name
	 */
	public String getProductName() {
		return productName;
	}

	/**
	 * returns Product's Quantity of the product
	 * 
	 * @return Product's Quantity
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * returns Product's price of the product
	 * 
	 * @return Product's Price
	 */
	public double getPrice() {
		return price;
	}

	@Override
	public boolean isSameProduct(long productId) {
		if (this.productId == productId)
			return true;
		return false;
	}

	@Override
	public boolean isSufficientQuantity(int quantity) {
		if (this.quantity >= quantity) {
			this.quantity -= quantity;
			return true;
		}
		return false;
	}

}
