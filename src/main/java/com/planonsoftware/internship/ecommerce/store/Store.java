package com.planonsoftware.internship.ecommerce.store;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * {@code Store} is used to initialize all the products present in the
 * store(warehouse)
 * 
 * @author Girish Gandham
 *
 */
public class Store {

	private final String inputFilePath = "\\input.txt";
	/**
	 * productList represents the list of products present in the store
	 */
	private final List<IProduct> productList;

	/**
	 * constructs the Store class and loads the products
	 */
	public Store() {
		productList = new ArrayList<IProduct>();
		try {
			loadProducts();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * returns List of products present in the store
	 * 
	 * @return Product List
	 */
	public List<IProduct> getProductList() {
		return productList;
	}

	/**
	 * loads all the products present in the store
	 * 
	 * @throws FileNotFoundException
	 */
	private void loadProducts() throws Exception {
		InputStream in = this.getClass().getResourceAsStream(inputFilePath);
		BufferedReader inputFile = new BufferedReader(new InputStreamReader(in));
		String input;
		while ((input = inputFile.readLine()) != null) {
			String[] inputs = input.split(";");
			Product p = new Product(Long.parseLong(inputs[0]), inputs[1], Integer.parseInt(inputs[2]),
					Double.parseDouble(inputs[3]));
			productList.add(p);
		}
		inputFile.close();
	}

	/**
	 * searches for the given Product Id in the store's list of product
	 * 
	 * @param productId Id to be searched
	 * 
	 * @return {@code IProduct} object if present
	 */
	public IProduct getProduct(long productId) {
		try {
			return productList.stream().filter(product -> ((Product) product).getProductId() == productId).findFirst()
					.get();
		} catch (Exception e) {
			return null;
		}
	}
}
