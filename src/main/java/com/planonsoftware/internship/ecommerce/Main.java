package com.planonsoftware.internship.ecommerce;

import com.planonsoftware.internship.ecommerce.store.Cart;
import com.planonsoftware.internship.ecommerce.store.IProduct;
import com.planonsoftware.internship.ecommerce.store.Store;

public class Main {
	public static void main(String[] args) {
		Store store = new Store();
		Cart cart = new Cart();
		System.out.println("Welcome to Online Shopping");
		Input input = new Input();
		char choice = 'y';
		do {
			long pid = input.getProductId();
			IProduct product = store.getProduct(pid);
			if (product == null) {
				System.out.println("Invalid Product Id");
				System.out.println("Please, enter valid Product Id");
				continue;
			}
			int quantity = input.getQuantity();
			if (!product.isSufficientQuantity(quantity)) {
				System.out.println("Out of Stock");
				System.out.println("Please, Enter another Product");
				continue;
			}
			cart.addToCart(product, quantity);
			choice = input.getChoice();
		} while (choice == 'y');

		cart.checkOut();
	}
}
