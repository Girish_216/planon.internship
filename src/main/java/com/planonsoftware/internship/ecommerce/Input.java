package com.planonsoftware.internship.ecommerce;

import java.util.Scanner;

public class Input {

	public Scanner input;

	public Input() {
		input = new Scanner(System.in);
	}

	/**
	 * to take choice from console
	 * 
	 * @return choice
	 */
	public char getChoice() {
		System.out.println("Do you want to Continue (y/n)");
		return input.nextLine().charAt(0);
	}

	/**
	 * to take Product ID from console
	 * 
	 * @return Product Id
	 */
	public long getProductId() {
		System.out.println("Enter Product id");
		return Long.parseLong(input.nextLine());
	}

	/**
	 * to take quantity from console
	 * 
	 * @return quantity
	 */
	public int getQuantity() {
		System.out.println("Enter Quantity of product:");
		return Integer.parseInt(input.nextLine());
	}
	
}
