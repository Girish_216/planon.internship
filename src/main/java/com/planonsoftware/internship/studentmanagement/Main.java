package com.planonsoftware.internship.studentmanagement;

import com.planonsoftware.internship.studentmanagement.data.Data;
import com.planonsoftware.internship.studentmanagement.data.EntityList;
import com.planonsoftware.internship.studentmanagement.data.InputFromFile;
import com.planonsoftware.internship.studentmanagement.model.College;
import com.planonsoftware.internship.studentmanagement.model.Department;
import com.planonsoftware.internship.studentmanagement.model.Student;

public class Main {

	/**
	 * main method
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		InputFromFile input = new InputFromFile("\\input1.txt");

		Data data = input.getData();

		EntityList<College> clglist = data.getCollegeList();
		System.out.println(clglist.getSucessfulEntires() + "  " + clglist.getUnSucessfulEntires());

		EntityList<Department> deptlist = data.getDepartmentList();
		System.out.println(deptlist.getSucessfulEntires() + "  " + deptlist.getUnSucessfulEntires());

		EntityList<Student> studentlist = data.getStudentList();
		System.out.println(studentlist.getSucessfulEntires() + "  " + studentlist.getUnSucessfulEntires());

		System.out.println(studentlist.printEntity("17501"));

	}
}
