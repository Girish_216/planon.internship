package com.planonsoftware.internship.studentmanagement.data;

import com.planonsoftware.internship.studentmanagement.model.College;
import com.planonsoftware.internship.studentmanagement.model.Department;
import com.planonsoftware.internship.studentmanagement.model.Entity;
import com.planonsoftware.internship.studentmanagement.model.Student;
import com.planonsoftware.internship.studentmanagement.model.EntityType;

/**
 * {@code Data} stores and retrieves data of students,departments and colleges
 * 
 * @author Girish Gandham
 *
 */
public class Data {

	/**
	 * list of Colleges
	 */
	private EntityList<College> collegeList;
	/**
	 * list of Departments
	 */
	private EntityList<Department> departmentList;
	/**
	 * list of Students
	 */
	private EntityList<Student> studentList;

	/**
	 * constructs Data class
	 */
	public Data() {
		this.collegeList = new EntityList<College>();
		this.departmentList = new EntityList<Department>();
		this.studentList = new EntityList<Student>();
	}

	/**
	 * returns College list
	 * 
	 * @return {@code List} of Colleges
	 */
	public EntityList<College> getCollegeList() {
		return collegeList;
	}

	/**
	 * returns Department list
	 * 
	 * @return {@code List} of Departments
	 */
	public EntityList<Department> getDepartmentList() {
		return departmentList;
	}

	/**
	 * returns Student list
	 * 
	 * @return {@code List} of Students
	 */
	public EntityList<Student> getStudentList() {
		return studentList;
	}

	/**
	 * returns Entity based on {@link EntityType}
	 * 
	 * @param type {@link EntityType}
	 * 
	 * @return Entity object
	 */
	public Entity getEntityByType(String type) {
		try {
			switch (EntityType.valueOf(type)) {
			case CLG:
				return new College(null, null, null);
			case DEPT:
				return new Department(null, null, 0, null);
			case STUDENT:
				return new Student(null, null, 0, null, null);
			default:
				collegeList.incrementUnSucessfulEntires();
				return null;
			}
		} catch (Exception e) {
			collegeList.incrementUnSucessfulEntires();
			return null;
		}
	}

	/**
	 * returns EntityList based on the {@link EntityType}
	 * 
	 * @param type {@link EntityType}
	 * 
	 * @return EntityList object
	 */
	public EntityList<?> getEntityListByType(EntityType type) {
		switch (type) {
		case CLG:
			return collegeList;
		case DEPT:
			return departmentList;
		case STUDENT:
			return studentList;
		default:
			return null;
		}
	}

	/**
	 * returns Parent EntityList of the given {@link EntityType}
	 * 
	 * @param type {@link EntityType}
	 * 
	 * @return EntityList object
	 */
	public EntityList<?> getParentEntityList(EntityType type) {
		switch (type) {
		case CLG:
			return null;
		case DEPT:
			return collegeList;
		case STUDENT:
			return departmentList;
		default:
			return null;
		}
	}

	/**
	 * adds a new Entity entry
	 * 
	 * @param inputs Entity details
	 */
	public void addEntity(String[] inputs) {
		Entity entity = getEntityByType(inputs[0].toUpperCase());
		try {
			if (entity instanceof College && inputs.length == 4) {
				collegeList.add(new College(inputs[1], inputs[2], inputs[3]));
				collegeList.incrementSucessfulEntires();
				return;
			} else if (entity instanceof Department && inputs.length == 5) {
				EntityList<?> parentlist = getParentEntityList(EntityType.valueOf(inputs[0].toUpperCase()));
				departmentList.add(new Department(inputs[1], inputs[2], Integer.parseInt(inputs[3]),
						(College) parentlist.getEntityByCode(inputs[4])));
				departmentList.incrementSucessfulEntires();
				return;
			} else if (entity instanceof Student && inputs.length == 6) {
				EntityList<?> parentlist = getParentEntityList(EntityType.valueOf(inputs[0].toUpperCase()));
				studentList.add(new Student(inputs[1], inputs[2], Integer.parseInt(inputs[3]), inputs[4],
						(Department) parentlist.getEntityByCode(inputs[5])));
				studentList.incrementSucessfulEntires();
				return;
			}
		} catch (Exception ex) {
			getEntityListByType(EntityType.valueOf(inputs[0].toUpperCase())).incrementUnSucessfulEntires();
		}
	}
}
