package com.planonsoftware.internship.studentmanagement.data;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * 
 * {@code InputData} is used to store data from file
 * 
 * @author Girish Gandham
 *
 */
public class InputFromFile {

	/**
	 * name of the input file
	 */
	private final String fileName;
	/**
	 * {@code Data} object used to store data
	 */
	private Data data;

	/**
	 * constructs InputData class
	 */
	public InputFromFile(String fileName) {
		data = new Data();
		this.fileName = fileName;
		try {
			loadDataFromFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Loads data from file
	 * 
	 * @throws IOException - if no file is present
	 */
	public void loadDataFromFile() throws IOException {
		InputStream in = this.getClass().getResourceAsStream(fileName);
		BufferedReader inputFile = new BufferedReader(new InputStreamReader(in));
		String input;
		while ((input = inputFile.readLine()) != null) {
			String[] inputs = input.split(",");
			data.addEntity(inputs);
		}
		inputFile.close();
	}

	/**
	 * returns Data object
	 * @return {@link Data} object
	 */
	public Data getData() {
		return data;
	}

}
