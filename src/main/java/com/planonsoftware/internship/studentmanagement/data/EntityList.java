package com.planonsoftware.internship.studentmanagement.data;

import java.util.ArrayList;
import java.util.List;

import com.planonsoftware.internship.studentmanagement.model.Entity;

public class EntityList<E extends Entity> {

	/**
	 * list of Entities
	 */
	private List<E> entityList;

	/**
	 * Number of successful entries
	 */
	private int sucessfulEntires;

	/**
	 * Number of unsuccessful entries
	 */
	private int unSucessfulEntires;

	/**
	 * constructs EntityList class
	 */
	public EntityList() {
		this.entityList = new ArrayList<E>();
		this.sucessfulEntires = 0;
		this.unSucessfulEntires = 0;
	}
	
	/**
	 * increments number of successful entries
	 * 
	 */
	public void incrementSucessfulEntires() {
		this.sucessfulEntires++;
	}

	/**
	 * increments number of unsuccessful entries
	 * 
	 */
	public void incrementUnSucessfulEntires() {
		this.unSucessfulEntires++;
	}

	/**
	 * sets number of successful entries
	 * 
	 * @param sucessfulEntires
	 */
	public void setSucessfulEntires(int sucessfulEntires) {
		this.sucessfulEntires = sucessfulEntires;
	}

	/**
	 * sets number of unsuccessful entries
	 * 
	 * @param unSucessfulEntires
	 */
	public void setUnSucessfulEntires(int unSucessfulEntires) {
		this.unSucessfulEntires = unSucessfulEntires;
	}

	/**
	 * returns Entity list
	 * 
	 * @return {@code List} of Entities
	 */
	public List<E> getEntityList() {
		return entityList;
	}

	/**
	 * returns number of successful entries
	 * 
	 * @return number of successful entries
	 */
	public int getSucessfulEntires() {
		return sucessfulEntires;
	}

	/**
	 * returns number of unsuccessful entries
	 * 
	 * @return number of unsuccessful entries
	 */
	public int getUnSucessfulEntires() {
		return unSucessfulEntires;
	}

	/**
	 * adds a new Entity entry
	 * 
	 * @param entiry
	 */
	public void add(E e) {
		entityList.add(e);
	}

	/**
	 * returns Entity of the given code
	 * 
	 * @param code Entity's code
	 * 
	 * @return {@code Entity} object
	 */
	public E getEntityByCode(String code) {
		return entityList.stream().filter((e) -> e.getCode().equals(code)).findFirst().get();
	}

	/**
	 * returns Entity details of the given code
	 * 
	 * @param code Entity's code
	 * 
	 * @return Entity details
	 */
	public String printEntity(String code) {
		try {
			return getEntityByCode(code).toString();
		} catch (Exception ex) {
			return "Invalid Code";
		}
	}
}
