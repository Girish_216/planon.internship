package com.planonsoftware.internship.studentmanagement.model;

/**
 * {@code Student} class stores all the details of a student
 * 
 * @author Girish Gandham
 *
 */
public class Student implements Entity{

	/**
	 * Student's code
	 */
	private final String code;
	/**
	 * name of the Student
	 */
	private final String name;
	/**
	 * Student's age
	 */
	private int age;
	/**
	 * Student's section
	 */
	private final String section;
	/**
	 * Student's Department
	 */
	private final Department dept;

	/**
	 * Constructs Student class
	 * 
	 * @param code
	 * @param name
	 * @param age
	 * @param section
	 * @param dept
	 */
	public Student(String code, String name, int age, String section, Department dept) {
		this.code = code;
		this.name = name;
		this.age = age;
		this.section = section;
		this.dept = dept;
	}

	@Override
	public String getCode() {
		return code;
	}

	/**
	 * returns Student's name
	 * 
	 * @return Student's name
	 */
	public String getName() {
		return name;
	}

	/**
	 * returns Student's age
	 * 
	 * @return Student's age
	 */
	public int getAge() {
		return age;
	}

	/**
	 * returns Student's section
	 * 
	 * @return Student's section
	 */
	public String getSection() {
		return section;
	}

	/**
	 * returns Student's Department
	 * 
	 * @return Student's Department
	 */
	public Department getDept() {
		return dept;
	}

	@Override
	public String toString() {
		return "Student code=" + code + ", name=" + name + ", age=" + age + ", section=" + section;
	}

}
