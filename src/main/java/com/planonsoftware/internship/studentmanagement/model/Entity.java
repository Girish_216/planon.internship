package com.planonsoftware.internship.studentmanagement.model;

/**
 * {@code Entity} basic interface for all entities
 * 
 * @author Girish Gandham
 *
 */
public interface Entity {

	/**
	 * returns Entity's code
	 * 
	 * @return Entity's code
	 */
	public String getCode();
}
