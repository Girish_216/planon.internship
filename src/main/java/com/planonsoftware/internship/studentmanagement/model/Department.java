package com.planonsoftware.internship.studentmanagement.model;

/**
 * {@code Department} class stores all department details
 * 
 * @author Girish Gandham
 *
 */
public class Department implements Entity{

	/**
	 * Department's code
	 */
	private final String code;
	/**
	 * Department's name
	 */
	private final String name;
	/**
	 * number of sections in the department
	 */
	private int numberOfSections;
	/**
	 * Department's College
	 */
	private final College clg;

	/**
	 * Constructs Department class
	 * 
	 * @param code
	 * @param name
	 * @param numberOfSections
	 * @param clg
	 */
	public Department(String code, String name, int numberOfSections, College clg) {
		this.code = code;
		this.name = name;
		this.numberOfSections = numberOfSections;
		this.clg = clg;
	}

	@Override
	public String getCode() {
		return code;
	}

	/**
	 * returns Department's name
	 * 
	 * @return Department's name
	 */
	public String getName() {
		return name;
	}

	/**
	 * returns number of sections in Department
	 * 
	 * @return number of sections in Department
	 */
	public int getNumberOfSections() {
		return numberOfSections;
	}

	/**
	 * returns Department's name
	 * 
	 * @return Department's name
	 */
	public College getClg() {
		return clg;
	}

	@Override
	public String toString() {
		return "\nDepartmentcode=" + code + ", Departmentname=" + name + ", numberOfSections=" + numberOfSections + ", "
				+ clg;
	}

}
