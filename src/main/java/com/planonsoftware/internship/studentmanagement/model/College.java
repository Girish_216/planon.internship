package com.planonsoftware.internship.studentmanagement.model;

/**
 * {@code College} class stores all college details
 * @author Girish Gandham
 *
 */
public class College implements Entity{

	/**
	 * College code
	 */
	private final String code;
	/**
	 * name of the College
	 */
	private final String name;
	/**
	 * College address
	 */
	private final String address;

	/**
	 * Constructs College class
	 * @param code
	 * @param name
	 * @param address
	 */
	public College(String code, String name, String address) {
		this.code = code;
		this.name = name;
		this.address = address;
	}

	@Override
	public String getCode() {
		return code;
	}

	/**
	 * returns College's name
	 * 
	 * @return College's name
	 */
	public String getName() {
		return name;
	}

	/**
	 * returns College's address
	 * 
	 * @return College's address
	 */
	public String getAddress() {
		return address;
	}

	@Override
	public String toString() {
		return "\nCollegecode=" + code + ", Collegename=" + name + ", Collegeaddress=" + address;
	}

}
