package com.planonsoftware.internship.studentmanagement.model;

/**
 * ENUM EntityType is used to specify type of Entity
 * @author Girish Gandham
 *
 */
public enum EntityType {
	CLG,DEPT,STUDENT;
}
