package com.planonsoftware.internship.studentmanagement.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import com.planonsoftware.internship.studentmanagement.data.Data;
import com.planonsoftware.internship.studentmanagement.data.InputFromFile;

class DataTest {

	@ParameterizedTest(name="inputfile{index}")
	@CsvSource(value = {
			"\\input1.txt#17501#17201:1#2#2#Student code=17501, name=girish, age=18, section=A#Invalid Code",
			"\\input2.txt#17501#17201:2#3#2#Student code=17501, name=girish, age=18, section=A#Invalid Code" }, delimiter = ':')
	void testFile(String inputString, String expectedString) {
		String[] inputs = inputString.split("#");
		InputFromFile input = new InputFromFile(inputs[0]);
		Data data = input.getData();
		String[] expected = expectedString.split("#");

		assertEquals(Integer.parseInt(expected[0]), data.getCollegeList().getSucessfulEntires());
		assertEquals(Integer.parseInt(expected[1]), data.getDepartmentList().getSucessfulEntires());
		assertEquals(Integer.parseInt(expected[2]), data.getStudentList().getSucessfulEntires());
		assertEquals(expected[3], data.getStudentList().printEntity(inputs[1]));
		assertEquals(expected[4], data.getStudentList().printEntity(inputs[2]));
	}

}
